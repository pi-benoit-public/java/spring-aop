# Projet avec Java avec Spring AOP

Site intéressant : [https://howtodoinjava.com](https://howtodoinjava.com)

Note libs à récupérer :

[Voir https://mvnrepository.com/artifact/org.aspectj/aspectjweaver/1.8.13](https://mvnrepository.com/artifact/org.aspectj/aspectjweaver/1.8.13)


## PointcutExpression

L'exécution est le paramètre entre parenthèse après un @Before, @After...

Exemples :

`@Before("execution(public void nom_method())")`

`@Before("execution(* fr.benoit.*.*(..))")` : ici exécute tous les sous-packages/classes, méthodes et paramètres dans le package `fr.benoit`
La 1ère étoile c'est le type de retour void ou autre.

`execution( modifiers_pattern ? return_type_pattern declaration_type_pattern ? method_name_pattern(param_pattern) throws pattern ?)`

? : pour ici indique que le paramètre précédent est optionnel

modifiers_pattern : public / private / protected

return_type_pattern : si * remplace void, String...

declaration_type_pattern : les classes (si on veut toutes les classes on met `*`)

method_name_pattern : obligatoire le nom de la méthode

param_pattern : `si () = 0 paramètre ; si (*) = 1 paramètre ; si (..) = 0 ou plusieurs paramètres`

throws pattern


Exemple avec Pointcut (se met dans la classe Aspect par exemple dans ce projet LoggingAspect)

```
@Pointcut("execution(*.fr.benoit.*.*(..))")
public void forDAOPackage() {}

// fait un "lien" avec l'exécution du Pointcut
@Before("forDAOPackage")
...

@After("forDAOPackage")
```

Possibilité de combiner avec `&& || !`

Exemple : `@Before("forDAOPackage && otherMethodAvecUnPointcutParExemple")`


`@Order(n)` : n -infni à +infini se met en annotation devant une classe Aspect. Permet de définir un ordre de priorité d'exécution.
