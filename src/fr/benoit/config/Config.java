package fr.benoit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import fr.benoit.dao.AccountDAO;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("fr.benoit")
public class Config {

	@Bean
	public AccountDAO account() {
		return new AccountDAO();
	};

}
