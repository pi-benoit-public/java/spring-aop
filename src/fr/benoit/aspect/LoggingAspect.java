package fr.benoit.aspect;

import org.aspectj.lang.annotation.After;
//import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	// addAccount from AccountDAO
	@Before("execution(public void addAccount())")
	public void thisBefore() {
	System.out.println("Ca se passe avant la m�thode addAccount() !");
	};

	@After("execution(public void addAccount())")
	public void thisAfter() {
		System.out.println("Ca se passe apr�s la m�thode addAccount().");
	}

//	@Around("execution(public void addAccount())")
//	public void thisAround() {
//		System.out.println("C'est autour.");
//	}

}
