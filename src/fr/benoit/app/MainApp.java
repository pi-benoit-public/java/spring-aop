package fr.benoit.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.benoit.config.Config;
import fr.benoit.dao.AccountDAO;

public class MainApp {

	public static void main(String[] args) {

		// Avec Spring annotation.
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

		AccountDAO accountDAO = context.getBean("account", AccountDAO.class);

		accountDAO.addAccount();

		context.close();

	}

}
